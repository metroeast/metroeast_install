#!/bin/bash
#
# some variables we can expect not to change often
dru_server=127.0.0.1
civ_server=127.0.0.1



## are we sudo or root?
this_uid=$(/usr/bin/id -u)
[ $this_uid -ne 0 ] && \
{
	echo "sudo required"
	exit -1
}



## variables/setup
yn[0]="no"
yn[1]="yes"
VERBOSE=0

log_file=/var/log/rebuild_$$.log
install_build=$(dirname "$0")




#######################
## functions
# simple logging tool
function log_event ()
{
	# write the date and time with message to stdout and a log file
	DATE_TIME=`date +'%Y-%m-%d %H:%M:%S'`
	# date-time [proc_id script] message  to  logfile
	echo "$DATE_TIME [$$] $1" >> "$log_file"
	# script: date-time message  to  stdout
	[ $VERBOSE -gt 0 ] && \
	{
		# echo
		echo "$DATE_TIME: $1"
	}
}


# read line wrap for log_event
function log_event_read ()
{
	while read this_line
	do
		log_event "$this_line"
	done
}


# main install
function install_it ()
{
	# prepare file system
	[ -d "$dst_site_root" ] && \
	{
		log_event "Removing $dst_site_root"
		rm -rf "$dst_site_root"
	}

	# drush make
	log_event "Preparing install (drush make)"
	log_event_read < <(
		drush make --prepare-install --force-complete "$install_build/metroeast_install.build" "$dst_site_root" -y 2>&1
	)


	# from the site root...
	cd "$dst_site_root"


	# Change file permissions/ownership on the site root
	## open permissions during install
	log_event "Permissions/Ownership"
	chmod -R 777 "$dst_site_root"
	chown -R _www:staff "$dst_site_root"


	# drush si
	log_event "Installing Drupal (drush si)"
	log_event_read < <(
		drush si metroeast_install --db-url=mysql://$dst_drup_user:$dst_drup_pass@$dru_server/$dst_drup_name --site-name="MetroEast" -y 2>&1
	)
	

	# add the 'live' database connection to settings.php
	# and the views integration civi database prefix
	#
	# protect '/' in password
	src_pass="${src_drup_pass//\//\\/}"
	blg_pass="${blg_drup_pass//\//\\/}"

	# protect '&' in password
	src_pass="${src_pass//\&/\\&}"
	blg_pass="${blg_pass//\&/\\&}"
	
	# make the file we will insert into settings.php
	temp_set_file=$(mktemp -t derp.XXXXXX)

	# edit placeholders
	sed -e "s/____views_integration_civi_database_prefix____/$dst_civi_name/g" \
		-e "s/____migration_source_database_name___/$src_drup_name/" \
		-e "s/____migration_source_database_user___/$src_drup_user/" \
		-e "s/____migration_source_database_pass___/$src_pass/" \
		-e "s/____blog_migration_source_database_name___/$blg_drup_name/" \
		-e "s/____blog_migration_source_database_user___/$blg_drup_user/" \
		-e "s/____blog_migration_source_database_pass___/$blg_pass/" \
		"$install_build/includes/metroeast_settings.php" > "$temp_set_file"

	# tack onto the end of settings.php
	log_event "Updating settings.php"
	echo >> "$dst_site_root/sites/default/settings.php"
	cat "$temp_set_file" >> "$dst_site_root/sites/default/settings.php"
	rm "$temp_set_file"


	## patch CiviDiscount from includes
	# source
	civi_discount_patch_root="$install_build/includes/org.civicrm.module.cividiscount"
	# destination
	civi_discount_exten_root="$dst_site_root/sites/all/civi_ext/org.civicrm.module.cividiscount"
	
	log_event "Patching CiviDiscount"
	log_event_read < <(
		cp -v "$civi_discount_patch_root/cividiscount.php" "$civi_discount_exten_root/cividiscount.php" 2>&1
		cp -v "$civi_discount_patch_root/CDM/Utils.php" "$civi_discount_exten_root/CDM/Utils.php" 2>&1
		cp -v "$civi_discount_patch_root/CDM/BAO/Item.php" "$civi_discount_exten_root/CDM/BAO/Item.php" 2>&1
	)
	
	
	## patch CiviCRM Multi-Day Event from includes
	# source
	civi_mde_patch_root="$install_build/includes/civicrm_multiday_event"
	# destination
	civi_mde_module_root="$dst_site_root/profiles/metroeast_install/modules/contrib/civicrm_multiday_event"
	
	log_event "Patching CiviCRM Multi-Day Event"
	log_event_read < <(
		cp -v "$civi_mde_patch_root/*" "$civi_mde_module_root/" 2>&1
	)
	
	
	## add custom php and templates for Civi
	# source
	civi_custom_parts_patch_root="$install_build/includes"
	# destination
	civi_custom_parts_dircetory="$dst_site_root/sites/all"
	
	log_event "Adding Custom Templates and PHP for CiviCRM"
	log_event_read < <(
		ditto -V "$civi_custom_parts_patch_root/custom_php/" "$civi_custom_parts_dircetory/custom_php/" 2>&1
		ditto -V "$civi_custom_parts_patch_root/custom_templates/" "$civi_custom_parts_dircetory/custom_templates/" 2>&1
	)
	
	
	[ $flag_ditto_files -eq 1 ] && \
	{
		## ditto files from source site to files folder set in metroeast_custom/modules/metroeast_migrate/metroeast_migrate.module
		# destination should be a variable, also stored in metroeast_migrate.module
		log_event "Ditto Files"
		log_event_read < <(
			ditto -v "$src_site_root/sites/default/files/" "/Volumes/Storage/Source/install/files/" 2>&1
		)
	} ## END flag_ditto_files


	[ $flag_ditto_blog_files -eq 1 ] && \
	{
		## ditto files from source site to files folder set in metroeast_custom/modules/metroeast_migrate/metroeast_migrate.module
		# destination should be a variable, also stored in metroeast_migrate.module
		log_event "Ditto Blog Files"
		log_event_read < <(
			ditto -v "$blg_site_root/sites/default/files/" "/Volumes/Storage/Source/install/blog_files/" 2>&1
		)
	} ## END flag_ditto_files


	## open permissions, prior to installing Civi
	chmod -R 777 "$dst_site_root"
	
	
	## curl CiviCRM install
	#
	log_event "Installing CiviCRM"
	result_html=$(curl -sS \
		--data-urlencode "mysql[server]=$civ_server" \
		--data-urlencode "mysql[username]=$dst_civi_user" \
		--data-urlencode "mysql[database]=$dst_civi_name" \
		--data-urlencode "mysql[password]=$dst_civi_pass" \
		--data-urlencode "drupal[server]=$dru_server" \
		--data-urlencode "drupal[username]=$dst_drup_user" \
		--data-urlencode "drupal[database]=$dst_drup_name" \
		--data-urlencode "drupal[password]=$dst_drup_pass" \
		--data-urlencode "go=Installing CiviCRM..." \
		--data-urlencode "database=MySQLDatabase" \
		--data-urlencode "seedLanguage=en_US" \
		"http://$dst_site_domain/sites/all/modules/civicrm/install/")
		
	success_test=$(echo "$result_html" | grep "CiviCRM has been successfully installed")
	[ "$success_test" == "" ] && \
	{
		log_event "ERROR: CiviCRM install failed"
		log_event "$result_html"
		log_event "ERROR: CiviCRM install failed: Exiting"
		exit 13
	} || {
		log_event "CiviCRM has been successfully installed"
	}
	

	log_event "Enabling MetroEast Custom"
	#
	log_event_read < <(
		drush en metroeast_custom -y 2>&1
	)
	# metroeast_custom installs several modules
	#
	
	
	
	## fix for fullcalendar library failing to 'install' in sites/all/libraries
	log_event "Copying fullcalendar library (workaround regarding path)"
	log_event_read < <(
    ditto "$dst_site_root/profiles/metroeast_install/libraries/fullcalendar-download/fullcalendar/" "$dst_site_root/sites/all/libraries/fullcalendar/"
  )


}
## END function install_it ()


function migrate_drupal ()
{
	# Migrate Drupal via drush / custom migration
	log_event "Migrating Drupal"

	#perform the data migrations
	log_event_read < <(
		drush mi --all 2>&1
	)
}
## END function migrate_drupal ()


function repair_perm ()
{
	## notice to user
	log_event "Repairing ownership and permissions..."


	## restore permissions
	root_dir_mode="u=rwx,go=rx"
	root_file_mode="u=rw,go=r"
	sites_dir_mode="ugo=rwx"
	files_dir_mode="ugo=rwx"
	files_file_mode="ug=rw,o=r"
	
	
	log_event_read < <(
		# from site root
		cd "$dst_site_root" 2>&1

		## ownership
		chown -R _www:staff "$dst_site_root" 2>&1

		## all files and directories
		find . -type d -exec chmod "$root_dir_mode" '{}' \; 2>&1
		find . -type f -exec chmod "$root_file_mode" '{}' \; 2>&1

		## specific to /sites/
		cd sites 2>&1
		find . -type d -name files -exec chmod "$sites_dir_mode" '{}' \; 2>&1
		for d in ./*/files
		do
			find $d -type d -exec chmod "$files_dir_mode" '{}' \; 2>&1
			find $d -type f -exec chmod "$files_file_mode" '{}' \; 2>&1
		done
	)
}
## END function repair_perm ()


function migrate_civi()
{
	## exporting CiviCRM from source
	log_event "Exporting CiviCRM"

	## more temp files
	temp_civ_data_file=$(mktemp -t civd.XXXXXX)


	## the data we want...  (no domain and no cache)
	log_event_read < <(
		mysqldump "$src_civi_name" \
			--user="$src_civi_user" \
			--password="$src_civi_pass" \
			--single-transaction \
			--ignore-table="$src_civi_name.civicrm_acl_cache" \
			--ignore-table="$src_civi_name.civicrm_acl_contact_cache" \
			--ignore-table="$src_civi_name.civicrm_cache" \
			--ignore-table="$src_civi_name.civicrm_group_contact_cache" \
			--ignore-table="$src_civi_name.civicrm_prevnext_cache" \
			--ignore-table="$src_civi_name.civicrm_domain" \
			--result-file="$temp_civ_data_file" 2>&1
	)


	## importing Civi
	log_event "Importing CiviCRM"
	
	## simple import
	log_event_read < <(
		mysql \
			--user="$dst_civi_user" \
			--password="$dst_civi_pass" \
			"$dst_civi_name" < "$temp_civ_data_file" 2>&1

		## cleanup
		rm "$temp_civ_data_file" 2>&1
	)
}
## END function migrate_civi()




##########################
### roll the parameters...

# peel 'em off one at a time...
while [ $# -gt 0 ]; do
	case "$1" in
		-h|"")
			cat <<HELP
Usage: $0 [-opt [sub-opt]]

	Note: The databases indicated must already exist
	Required: *

--help     exteneded help

--verbose          -v      log to stdout

--src-site-root    -sr     source site root server path
--blg-site-root    -br     blog migration source site root server path

--dst-site-domain  -dd     * destination site domain
--dst-site-root    -dr     * destination site root server path


--src-drup-user    -sdu    source drupal database user
--src-drup-pass    -sdp    source drupal database password
--src-drup-name    -sdn    source drupal database name

--blg-drup-user    -bdu    blog source drupal database user
--blg-drup-pass    -bdp    blog source drupal database password
--blg-drup-name    -bdn    blog source drupal database name

--dst-drup-user    -ddu    * destination drupal database user
--dst-drup-pass    -ddp    * destination drupal database password
--dst-drup-name    -ddn    * destination drupal database name

--src-civi-user    -scu    source civicrm database user
--src-civi-pass    -scp    source civicrm database password
--src-civi-name    -scn    source civicrm database name

--dst-civi-user    -dcu  * destination civicrm database user
--dst-civi-pass    -dcp  * destination civicrm database password
--dst-civi-name    -dcn  * destination civicrm database name

HELP
			exit 86
			;;
			
		--help)
			cat <<LONGHELP
Drupal/Civi Build and Migrate
Usage: $0 [-opt [sub-opt]]

	Note: The databases indicated must already exist

	Required: *


Source Server Path / Site Root
	ditto /sites/default/files/ to the install source /files/
		omitting skips the ditto

	-sr "/Volumes/Storage/Sites/www.metroeast.org"
	
	-br "/Volumes/Storage/Sites/blog.metroeast.org"


* Destination Site Domain, Server Path / Site Root
	define the location and domain name for the build
	
	domain
	-dd "dev3.metroeast.org"
	
	server path
	-dr "/Volumes/Storage/Sites/dev3.metroeast.org"
	


Source Drupal Database
	define the migration source credentials
		omitting skips the migration
	
	user
	-sdu "bob"
	
	password
	-sdp "derp"
	
	name
	-sdn "bob_database"


Blog Source Drupal Database
	define the migration source credentials
		omitting skips the migration
	
	user
	-bdu "bob"
	
	password
	-bdp "derp"
	
	name
	-bdn "bob_database"


* Destination Drupal Database
	define the database for the build, migration destination

	user
	-ddu "newb"
	
	password
	-ddp "lame"
	
	name
	-ddn "newb_database"



Source CiviCRM Database
	define the migration source credentials
		omitting skips the migration
	
	user
	-scu "bob"
	
	password
	-scp "derp"
	
	name
	-scn "bob_database"


* Destination CiviCRM Database
	define the database for the build, migration destination

	user
	-dcu "newb"
	
	password
	-dcp "lame"
	
	name
	-dcn "newb_database"



LONGHELP
			exit 86
			;;

		--verbose|-v)
			# log to stdout and log file
			VERBOSE=1
			;;

		--src-site-root|-sr)
			# source site root server path
			case $2 in
				-*|"")
					log_event "ERR: missing string 'source site root server path', exiting..."
					exit 1
					;;
				*)
					src_site_root="$2"
					shift
					;;
			esac
			;;
			
		--blg-site-root|-br)
			# source site root server path
			case $2 in
				-*|"")
					log_event "ERR: missing string 'blog source site root server path', exiting..."
					exit 1
					;;
				*)
					blg_site_root="$2"
					shift
					;;
			esac
			;;
			
		--dst-site-domain|-dd)
			# destination site domain
			case $2 in
				-*|"")
					log_event "ERR: missing string 'destination site domain', exiting..."
					exit 1
					;;
				*)
					dst_site_domain="$2"
					shift
					;;
			esac
			;;
			
		--dst-site-root|-dr)
			# destination site root server path
			case $2 in
				-*|"")
					log_event "ERR: missing string 'destination site root server path', exiting..."
					exit 1
					;;
				*)
					dst_site_root="$2"
					shift
					;;
			esac
			;;
			
		--src-drup-user|-sdu)
			# source drupal database user
			case $2 in
				-*|"")
					log_event "ERR: missing string 'source drupal database user', exiting..."
					exit 1
					;;
				*)
					src_drup_user="$2"
					shift
					;;
			esac
			;;
			
		--src-drup-pass|-sdp)
			# source drupal database password
			case $2 in
				-*|"")
					log_event "ERR: missing string 'source drupal database password', exiting..."
					exit 1
					;;
				*)
					src_drup_pass="$2"
					shift
					;;
			esac
			;;
			
		--src-drup-name|-sdn)
			# source drupal database name
			case $2 in
				-*|"")
					log_event "ERR: missing string 'source drupal database name', exiting..."
					exit 1
					;;
				*)
					src_drup_name="$2"
					shift
					;;
			esac
			;;
			
		--blg-drup-user|-bdu)
			# blog source drupal database user
			case $2 in
				-*|"")
					log_event "ERR: missing string 'blog source drupal database user', exiting..."
					exit 1
					;;
				*)
					blg_drup_user="$2"
					shift
					;;
			esac
			;;
			
		--blg-drup-pass|-bdp)
			# blog source drupal database password
			case $2 in
				-*|"")
					log_event "ERR: missing string 'blog source drupal database password', exiting..."
					exit 1
					;;
				*)
					blg_drup_pass="$2"
					shift
					;;
			esac
			;;
			
		--blg-drup-name|-bdn)
			# blog source drupal database name
			case $2 in
				-*|"")
					log_event "ERR: missing string 'blog source drupal database name', exiting..."
					exit 1
					;;
				*)
					blg_drup_name="$2"
					shift
					;;
			esac
			;;
			
		--dst-drup-user|-ddu)
			# destination drupal database user
			case $2 in
				-*|"")
					log_event "ERR: missing string 'destination drupal database user', exiting..."
					exit 1
					;;
				*)
					dst_drup_user="$2"
					shift
					;;
			esac
			;;
			
		--dst-drup-pass|-ddp)
			# destination drupal database password
			case $2 in
				-*|"")
					log_event "ERR: missing string 'destination drupal database password', exiting..."
					exit 1
					;;
				*)
					dst_drup_pass="$2"
					shift
					;;
			esac
			;;
			
		--dst-drup-name|-ddn)
			# destination drupal database name
			case $2 in
				-*|"")
					log_event "ERR: missing string 'destination drupal database name', exiting..."
					exit 1
					;;
				*)
					dst_drup_name="$2"
					shift
					;;
			esac
			;;
			
		--src-civi-user|-scu)
			# source civicrm database user
			case $2 in
				-*|"")
					log_event "ERR: missing string 'source civicrm database user', exiting..."
					exit 1
					;;
				*)
					src_civi_user="$2"
					shift
					;;
			esac
			;;
			
		--src-civi-pass|-scp)
			# source civicrm database password
			case $2 in
				-*|"")
					log_event "ERR: missing string 'source civicrm database password', exiting..."
					exit 1
					;;
				*)
					src_civi_pass="$2"
					shift
					;;
			esac
			;;
			
		--src-civi-name|-scn)
			# source civicrm database name
			case $2 in
				-*|"")
					log_event "ERR: missing string 'source civicrm database name', exiting..."
					exit 1
					;;
				*)
					src_civi_name="$2"
					shift
					;;
			esac
			;;
			
		--dst-civi-user|-dcu)
			# destination civicrm database user
			case $2 in
				-*|"")
					log_event "ERR: missing string 'destination civicrm database user', exiting..."
					exit 1
					;;
				*)
					dst_civi_user="$2"
					shift
					;;
			esac
			;;
			
		--dst-civi-pass|-dcp)
			# destination civicrm database password
			case $2 in
				-*|"")
					log_event "ERR: missing string 'destination civicrm database password', exiting..."
					exit 1
					;;
				*)
					dst_civi_pass="$2"
					shift
					;;
			esac
			;;
			
		--dst-civi-name|-dcn)
			# destination civicrm database name
			case $2 in
				-*|"")
					log_event "ERR: missing string 'destination civicrm database name', exiting..."
					exit 1
					;;
				*)
					dst_civi_name="$2"
					shift
					;;
			esac
			;;
		
		*)
			# default...  show ???
			log_event "I don't understand: $1"
			exit 13
			;;
	esac
	shift
done

## confirm what we're doing, report missing items
#

## required items
[ "$dst_site_domain" == "" ] && {
	log_event "REQUIRED: 'destination site domain' missing"
	exit 1
}
[ "$dst_site_root" == "" ] && {
	log_event "REQUIRED: 'destination site root server path' missing"
	exit 1
}
[ "$dst_drup_user" == "" ] && {
	log_event "REQUIRED: 'destination drupal database user' missing"
	exit 1
}
[ "$dst_drup_pass" == "" ] && {
	log_event "REQUIRED: 'destination drupal database password' missing"
	exit 1
}
[ "$dst_drup_name" == "" ] && {
	log_event "REQUIRED: 'destination drupal database name' missing"
	exit 1
}
[ "$dst_civi_user" == "" ] && {
	log_event "REQUIRED: 'destination civicrm database user' missing"
	exit 1
}
[ "$dst_civi_pass" == "" ] && {
	log_event "REQUIRED: 'destination civicrm database password' missing"
	exit 1
}
[ "$dst_civi_name" == "" ] && {
	log_event "REQUIRED: 'destination civicrm database name' missing"
	exit 1
}


## optional items

# ditto /files/ ??
flag_ditto_files=0
[ "$src_site_root" != "" ] && {
	## yes
	flag_ditto_files=1
}

# blog files
flag_ditto_blog_files=0
[ "$blg_site_root" != "" ] && {
	## yes
	flag_ditto_blog_files=1
}

# migrate drupal ??
flag_migr_drupal=0
[ "$blg_drup_user" != "" ] && [ "$blg_drup_pass" != "" ] && [ "$blg_drup_name" != "" ] && \
[ "$src_drup_user" != "" ] && [ "$src_drup_pass" != "" ] && [ "$src_drup_name" != "" ] && {
	## yes
	flag_migr_drupal=1
}

# migrate civicrm ??
flag_migr_civicrm=0
[ "$src_civi_user" != "" ] && [ "$src_civi_pass" != "" ] && [ "$src_civi_name" != "" ] && {
	## yes
	flag_migr_civicrm=1
}



##############
###  MAIN  ###

## confirm settings passed
cat<<MESSAGE
Please confirm parameters.
    Source --->
        Destination

Build Site Domain:
    $dst_site_domain

Site Root Path:
    $src_site_root --->
    $blg_site_root --->
        $dst_site_root

Drupal:
  user:
    $src_drup_user --->
b   $blg_drup_user --->
        $dst_drup_user
  name:
    $src_drup_name --->
b   $blg_drup_name --->
        $dst_drup_name

CiviCRM:
  user:
    $src_civi_user --->
        $dst_civi_user
  name:
    $src_civi_name --->
        $dst_civi_name


       Ditto /files/ ?   ${yn[$flag_ditto_files]}
  Ditto blog /files/ ?   ${yn[$flag_ditto_files]}
      Migrate Drupal ?   ${yn[$flag_migr_drupal]}
     Migrate CiviCRM ?   ${yn[$flag_migr_civicrm]}


Press [Enter] to continue.
MESSAGE


## prompt
read

echo "Starting..."
echo
echo "logging to: $log_file"
echo

## install
install_it


## migrate
[ $flag_migr_civicrm -eq 1 ] && {
  migrate_civi
  # drush en features_civicrm -y
}
[ $flag_migr_drupal -eq 1 ] && migrate_drupal


## clear cache
log_event "Clearing Cache"

#finally, clear the drupal cache
log_event_read < <(
	drush cc all 2>&1
)


:<<NOPE
[ $flag_ditto_blog_files -eq 1 ] && \
{
  ## ditto files for blog content
  log_event "Ditto Blog Files into new site"
  log_event_read < <(
    ditto -v "/Volumes/Storage/Source/install/blog_files/" "$dst_site_root/sites/default/files/" 2>&1
  )
} ## END flag_ditto_files
NOPE


## permissions
repair_perm


# 'fix' for permissions which show as overridden and have missing bits...
# might consider altering dependencies (may need role to exist)
## roles that work without this: anonymous user, authenticated user, administrator
## all other role permisssions fail
# this sorts it... for now.
log_event "Toggling metroeast_custom to sort permissions."
log_event_read < <(
	drush dis metroeast_custom -y 2>&1
	drush en metroeast_custom -y 2>&1
)


# repair session dates
# open and save all civicrm multiday events
log_event "Repairing CiviCRM Multiday Event session dates and display formats."
log_event_read < <(
	drush php-eval "_metroeast_custom_reset_civi_events();" 2>&1
)


# activate a few blocks
# writes to block region
log_event "Loading block regions."
log_event_read < <(
	drush php-eval "_metroeast_block_region_enable_blocks('metroeast_zen');" 2>&1
)


log_event_read < <(
cat<<LASTBIT

Finish CiviCRM migration, update URIs and paths:
http://$dst_site_domain/civicrm/admin/setting/path?reset=1
  $dst_site_root/sites/all/custom_templates/
  $dst_site_root/sites/all/custom_php/
  $dst_site_root/sites/all/civi_ext/

http://$dst_site_domain/civicrm/admin/setting/url?reset=1
  $dst_site_domain/profiles/metroeast_install/themes/metroeast_zen/civioverride/civioverride.css

http://$dst_site_domain/civicrm/admin/setting/updateConfigBackend?reset=1
  (cleanup caches)


Done!

LASTBIT
)

echo "Done."

exit 0
